import 'package:flutter/material.dart';
import 'views/login_view.dart';  

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => const MaterialApp(
        title: 'P1 IPC',
        debugShowCheckedModeBanner: false,  
        home: LoginView(),
      );
}
