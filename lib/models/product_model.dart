import 'dart:convert';

class Product {
  final String id;
  final String name;
  final double price;
  final String? iconPath;
  final String? description; 

  Product({
    required this.id,
    required this.name,
    required this.price,
    this.iconPath,
    this.description,
  });

  factory Product.fromJson(Map<String, dynamic> json) {
    return Product(
      id: json['id'],
      name: json['name'],
      price: json['price'].toDouble(),
      iconPath: json['iconPath'],
      description: json['description'],
    );
  }

  static List<Product> fromJsonList(List<dynamic> jsonList) {
    return jsonList.map((json) => Product.fromJson(json)).toList();
  }
}