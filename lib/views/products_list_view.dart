 import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:ysemkas_p1/models/product_model.dart';
import 'package:ysemkas_p1/views/product_details_view.dart';
import 'dart:convert';

class ProductsListView extends StatelessWidget {
  Future<List<Product>> _loadProducts() async {
    try {
      final jsonString = await rootBundle.loadString('assets/products.json');
      final List<dynamic> productList = json.decode(jsonString);
      return Product.fromJsonList(productList);
    } catch (e) {
      print(e.toString());
      return [];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Productos'),
        backgroundColor: Colors.purple,
        foregroundColor: Colors.white,
      ),
      body: FutureBuilder<List<Product>>(
        future: _loadProducts(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          } else if (snapshot.hasError) {
            return Center(child: Text('Error: ${snapshot.error}'));
          } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
            return Center(child: Text('No hay productos disponibles'));
          } else {
            final products = snapshot.data!;
            return ListView.builder(
              itemCount: products.length,
              itemBuilder: (context, index) {
                final product = products[index];
                return Card(
                  margin: EdgeInsets.all(8.0),
                  child: ListTile(
                    contentPadding: EdgeInsets.all(8.0),
                    leading: Hero(
                      tag: 'hero-product-${product.id}',
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: Image.asset(product.iconPath ?? 'assets/placeholder.png', width: 80, height: 80),
                      ),
                    ),
                    title: Text(product.name, style: TextStyle(fontWeight: FontWeight.bold)),
                    subtitle: Text('\$${product.price.toStringAsFixed(2)}\n${product.description ?? "No description available."}', style: TextStyle(color: Colors.grey[600])),
                    isThreeLine: true,
                    trailing: Icon(Icons.arrow_forward_ios),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ProductDetailsView(product: product),
                        ),
                      );
                    },
                  ),
                );
              },
            );
          }
        },
      ),
    );
  }
}
