import 'package:flutter/material.dart';
import 'package:ysemkas_p1/models/product_model.dart';

class ProductDetailsView extends StatelessWidget {
  final Product product;

  ProductDetailsView({required this.product});

 // ProductDetailsView
 
@override
Widget build(BuildContext context) {
  return Scaffold(
    appBar: AppBar(
      title: Text('Detalles del producto'),
      backgroundColor: Colors.purple,
      foregroundColor: Colors.white,
    ),
    body: SingleChildScrollView( 
      padding: EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 20.0),
          Hero(
            tag: 'hero-product-${product.id}',
            child: ClipRRect(
              borderRadius: BorderRadius.circular(8.0),
              child: Image.asset(
                product.iconPath ?? 'assets/placeholder.png',
                fit: BoxFit.cover,
                width: MediaQuery.of(context).size.width, // Ancho de pantalla completa
                height: 200, // Altura fija para la imagen
              ),
            ),
          ),
          SizedBox(height: 20.0),
          Text(
            product.name,
            style: TextStyle(
              fontSize: 28.0,
              fontWeight: FontWeight.bold,
            ),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 10.0),
          Text(
            '\$${product.price.toStringAsFixed(2)}',
            style: TextStyle(
              fontSize: 22.0,
              fontWeight: FontWeight.w500,
              color: Colors.green[700], 
            ),
          ),
          SizedBox(height: 20.0),
          Text(
            product.description ?? 'No description provided.',
            style: TextStyle(
              fontSize: 16.0,
            ),
          ),
          SizedBox(height: 30.0),
          ElevatedButton(

            // Botón agregar al carrito

            onPressed: () {},
            child: Text('Agregar al carrito'),
            style: ElevatedButton.styleFrom(
              minimumSize: Size(double.infinity, 50), // Ancho completo y altura fija
            ),
          ),
        ],
      ),
    ),
  );
}

}